'use strict';

var gulp = require('gulp');
var fileinclude = require('gulp-file-include');
var ext_replace = require('gulp-ext-replace');
var watch = require('gulp-watch');
var del = require('del');

var runSequence = require('run-sequence');

var source = './source/**/*.htm'

gulp.task('clean', function () {
    del(['./output/**/*.*']);
});

gulp.task('fileinclude', function () {
    return gulp.src('./source/' + '*.htm')
        .pipe(fileinclude('@@'))
        .pipe(ext_replace('.txt'))
        .pipe(gulp.dest('./output/'));
});

gulp.task('default', function(){
    gulp.start('clean');
    gulp.start('fileinclude');

    gulp.watch(source, function(){
        gulp.start('fileinclude');
    })
})